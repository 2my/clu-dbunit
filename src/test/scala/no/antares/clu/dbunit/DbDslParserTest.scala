/*******************************************************************************
 * Copyright 2013 Tommy Skodje (Antares Gruppen AS - http://www.antares.no)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package no.antares.clu.dbunit

import java.io.ByteArrayOutputStream
import java.io.File
import java.io.PrintStream
import java.io.PrintWriter
import scala.collection.mutable.ListBuffer
import org.hamcrest.Matchers.containsString
import org.hamcrest.Matchers.is
import org.junit.Assert.assertThat
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.mock.MockitoSugar
import org.scalatest.BeforeAndAfterAll
import java.sql.DriverManager
import java.util.Properties


/**  */
@RunWith(classOf[JUnitRunner])
class DbDslParserTest extends DbDslParser with FunSuite with BeforeAndAfter with BeforeAndAfterAll with MockitoSugar {

  val parser	= new DbDslParser();
  def parseUser( t: CharSequence ) = parser.parseAll( parser.user, t );
	var files2delete	= new ListBuffer[File]();

	override def beforeAll(configMap: Map[String, Any]) {
		val driver = "org.apache.derby.jdbc.EmbeddedDriver";
		Class.forName(driver).newInstance();
		val props = new Properties();
		props.put( "user", "tommy" );
		props.put( "username", "tommy" );
		props.put( "password", "tommy was here" );
		DriverManager.getConnection( "jdbc:derby:testDb;create=true", props);
	}
	override def afterAll(configMap: Map[String, Any]) {
	  try {
	  	DriverManager.getConnection("jdbc:derby:;shutdown=true");
	  } catch {
	    case t: Throwable =>;
	  }
	}
	after {
	  files2delete.foreach { file =>
	    file.delete()
	  }
	  files2delete.clear();
	}

  test( "user parses entire quoted string" ) {
    assertThat ( parseUser( "user: \"tommy was here\"" ).get, is( "tommy was here" ) );
  }
  test( "user handles space" ) {
    assertThat ( parseUser( "user: tommy" ).get, is( "tommy" ) );
  }
  test( "user handles no space" ) {
    assertThat ( parseUser( "user:tommy" ).get, is( "tommy" ) );
  }

  test( "sql parses quoted string" ) {
    Console.println( parser.parseAll( parser.sql, "\"A\"" ) )
    val result  = parser.parseAll( parser.sql, "\"A\"" )
    assertThat ( result.get, is( "A" ) );
  }
  test( "properties parses complete spec" ) {
  	val cmd	= "user:tommy password: \"tommy was here\" at:jdbc:derby:testDb class:org.apache.derby.jdbc.EmbeddedDriver"

  	val result  = parser.parseAll( parser.properties, cmd ).get

  	val expected	= DbSpec( "tommy", "tommy was here", "jdbc:derby:testDb", "org.apache.derby.jdbc.EmbeddedDriver", "tommy" );
  	assertThat ( result, is( expected ) );
  }

  test( "properties fails when spec incomplete" ) {
  	val cmd	= "user:tommy password:secret class:jdbc.EmbeddedDriver"
    assertThat ( parser.parseAll( parser.properties, cmd ).successful, is( false ) );
  }

  test( "writeProps streams properties to stdout" ) {
    val result  = toPropsParse( "writeProps " + dbProps( TestDb ) );
		assertThat ( result, is( TestDb.dbProperties.toString ) );
  }
  test( "writeProps streams properties to stdout - fully written" ) {
    val result  = toPropsParse( "writeProps user:TOMMY password: \"hemmelig passord\" at:jdbc:derby:derbyDB;create=true class:org.apache.derby.jdbc.EmbeddedDriver" );
		assertThat ( result, is( TestDb.dbProperties.toString ) );
  }

  test( "propFile reads property file" ) {
    val cmdInFile	= toPropsParse( "writeProps " + dbProps( TestDb ) )
    val f	= toFile( cmdInFile );
  	val cmd	= "propFile: " + f.getName()

  	val result  = parser.parseAll( parser.propFile, cmd ).get

    assertThat ( result, is( TestDb.dbProperties ) );
  }

  test( "export streams selected data as json to stdout" ) {
    TestDb.insert( -11, "42" )
    val cmd	= String.format( "export %s dump json \"%s\"", dbProps(TestDb), TestDb.select( -11 ) );

    val result	= exportParse( cmd )

  	assertThat ( result, containsString( """"@VALUE": "42"""" ) );
  }

  test( "export streams selected data as xml to stdout" ) {
    // FixMe
  }

  test( "refresh updates database" ) {
    TestDb.resetDb()
    val tstString	= TstString( -42 );

    parser.parseAll( parser.refresh, refresh( tstString ) )

  	val jsonFromDb	= exportParse( String.format( "export %s dump json \"%s\"", dbProps(TestDb), tstString.select ) )
  	assertThat ( jsonFromDb, containsString( tstString.value ) );
  }

  test( "command does export" ) {
    TestDb.insert( -5, "dumme dumme dum" )
  	val result	= commandParse( String.format( "export %s dump json \"%s\"", dbProps(TestDb), TestDb.select( -5 ) ) )
  	assertThat ( result, containsString( """"@VALUE": "dumme dumme dum"""" ) );
  }
  test( "command does refresh" ) {
    TestDb.resetDb()
    val tstString	= TstString( -555 );

    parser.parseAll( parser.refresh, refresh( tstString ) )

  	val jsonFromDb	= commandParse( String.format( "export %s dump json \"%s\"", dbProps(TestDb), tstString.select ) )
  	assertThat ( jsonFromDb, containsString( tstString.value ) );
  }

  test( "command does toProps" ) {
    val result  = commandParse( "writeProps " + dbProps( TestDb ) )
		assertThat ( result, is( TestDb.dbProperties.toString ) );
  }

  test( "main" ) {
    val main	= new Main( new DbDslParser() )
    val result = captureSystemOut { () => main.perform( "writeProps user:u password:p at:jdbc:derby:testDb class:org.apache.derby.jdbc.EmbeddedDriver".split( " " ) ) }
    assertThat ( result, is( DbSpec( "u", "p", "jdbc:derby:testDb", "org.apache.derby.jdbc.EmbeddedDriver", "u" ).toString ) );

    val command	= "writeProps " + dbProps( TestDb )
    val fails = captureSystemOut { () => main.perform( command.split( " " ) ) }
    // assertThat ( result, is( TestDb.dbProperties.toString ) );
  }


  //---------------------------------------------------------------------------
  private def refreshParse( cmd: String ) = captureSystemOut { () => parser.parseAll( parser.refresh, cmd ) };
  private def exportParse( cmd: String ) = captureSystemOut { () => parser.parseAll( parser.export, cmd ) };
  private def toPropsParse( cmd: String ) = captureSystemOut { () => parser.parseAll( parser.writeProps, cmd ) };
  private def commandParse( cmd: String ) = captureSystemOut { () => parser.parse( cmd, None ) };

  private def captureSystemOut( proc: () => Unit ): String = {
    val os	= new ByteArrayOutputStream();
    Console.setOut( new PrintStream( os ) );
    proc()
    Console.setOut( System.out );
    os.close()
    os.toString();
  }

  private case class TstString(
      	val id: Int	= -1,
      	val value: String	= "refreshed at " + System.currentTimeMillis().toString
      ) {
    val asJson	= String.format( "{'dataset': {'tstStrings': [{ 'id': '%s', 'value': '%s' }] } }", id.toString, value )
    val select	= TestDb.select( id )
  };

  private def refresh( tstString: TstString ): String = String.format( "refresh %s from json \"%s\"", dbProps(TestDb), tstString.asJson );

  private def dbProps( db: TestDb ): String = {
	  String.format( "user:%s password:\"%s\" at:%s class:%s", db.username, db.password, db.dbUrl, db.driver );
	}

	private def toFile( s: String ): File = {
	  val f	= createFile( "TESTFILE_" + System.currentTimeMillis() )
	  toFile( f, s )
	  f
	}
	private def toFile( f: File, s: String ): Unit = printToFile( f )( p => { p.print( s ) } )
	private def printToFile(f: File)(op: PrintWriter => Unit) {
	  val p = new java.io.PrintWriter(f)
	  try { op(p) } finally { p.close() }
	}

	def createFile( name: String ): File = {
	  val file	= new File( "TESTFILE_" + System.currentTimeMillis() );
	  files2delete  += file
	  file
	}

}
