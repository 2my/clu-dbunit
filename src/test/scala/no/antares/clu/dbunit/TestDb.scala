/*******************************************************************************
 * Copyright 2013 Tommy Skodje (Antares Gruppen AS - http://www.antares.no)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package no.antares.clu.dbunit

/* TestDb.scala
   Copyright 2012 Tommy Skodje (http://www.antares.no)

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
import org.slf4j.{LoggerFactory, Logger}
import org.apache.derby.tools.ij
import org.dbunit.database.DatabaseConfig
import java.io.ByteArrayInputStream
import no.antares.dbunit.DbWrapper

/** 
 * @author tommy skodje
*/
class TestDb extends no.antares.dbunit.DbProperties(
    "org.apache.derby.jdbc.EmbeddedDriver",
    "jdbc:derby:derbyDB;create=true",
    "TOMMY",
    "hemmelig passord",
    "TOMMY"
) {
  private val logger: Logger = LoggerFactory.getLogger( classOf[TestDb] )
  dbUnitProperties.append( ( DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, false.asInstanceOf[Object] ) )

	override def runSqlScript( script: String ): Boolean = {
	  try {
	    val input  = new ByteArrayInputStream( script.getBytes() )
	    val result  = ij.runScript( dbConnection, input, "UTF-8", System.out, "UTF-8" );
	    logger.debug( "ij.runScript, result code is: " + result );
	    return (result==0);
	  } catch {
	    case t: Throwable => return false;
	  }
	}

}

object TestDb extends TestDb {
  val dbProperties	= new DbSpec( username, password, dbUrl, driver )

  val sqlDropScript	= "drop table tst_strings"
	val sqlCreateScript	= """create table tst_strings (
			ID integer PRIMARY KEY,
			VALUE varchar(255)
		);
		""";

  runSqlScripts( sqlCreateScript );

  def resetDb() = runSqlScripts( sqlDropScript, sqlCreateScript );

  def insert( id: Int, value: String ): Boolean = {
    val cmd	= String.format( "insert into tst_strings ( id, value ) values ( %s, '%s' )", id.toString, value )
    runSqlScript( cmd );
  }

  def select( id: Int )	= "select * from tst_strings where id=" + id;

  def unWrappedJsonTestData( value: String )	= """{
    "tstStrings": [
      { "value": "Value1"  }
    ]
  }""".replaceAll( "Value1", value )

  val flatXmlTestData	= """<?xml version='1.0' encoding='UTF-8'?>
		<dataset>
		  <tst_strings ID="0" VALUE="GODKJENT" />
		  <tst_strings ID="1" VALUE="28%" />
		  <tst_strings ID="2" VALUE="Up&#229;klagelig" />
		  <tst_strings ID="3" VALUE="13.5/19.5%" />
		</dataset>"""
		;

}
