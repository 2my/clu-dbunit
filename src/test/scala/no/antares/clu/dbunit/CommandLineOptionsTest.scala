/*******************************************************************************
 * Copyright 2013 Tommy Skodje (Antares Gruppen AS - http://www.antares.no)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package no.antares.clu.dbunit

import org.hamcrest.Matchers.is
import org.junit.Assert.assertThat
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.mock.MockitoSugar


/**  */
@RunWith(classOf[JUnitRunner])
class CommandLineOptionsTest extends FunSuite with BeforeAndAfter with MockitoSugar {

  test( "parse nothing" ) {
    assertThat ( CommandLineOptions.parse2dsl( Array() ), is( "" ) )
  }
  test( "parse blank" ) {
    assertThat ( CommandLineOptions.parse2dsl( Array( "" ) ), is( "" ) )
  }
  test( "parse space" ) {
    assertThat ( CommandLineOptions.parse2dsl( Array( " " ) ), is( "" ) )
  }

  test( "parse unspaced strings" ) {
    assertThat ( CommandLineOptions.parse2dsl( Array( "-a", "1" ) ), is( "-a 1" ) )
  }
  test( "parse quotes spaced strings" ) {
    assertThat ( CommandLineOptions.parse2dsl( Array( "values:", "1 2, 34" ) ), is( "values: \"1 2, 34\"" ) )
  }
  test( "parse escapes quoted strings" ) {
    assertThat ( CommandLineOptions.parse2dsl( Array( "values:", "oh \"really!" ) ), is( "values: \"oh \\\"really!\"" ) )
	}
  test( "parse singlequoted strings" ) {
    assertThat ( CommandLineOptions.parse2dsl( Array( "values:", "'oh really!'" ) ), is( "values: \"'oh really!'\"" ) )
  }

}
