/*******************************************************************************
 * Copyright 2013 Tommy Skodje (Antares Gruppen AS - http://www.antares.no)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package no.antares.clu.dbunit

import java.io.File
import scala.io.Source
import scala.util.parsing.combinator.JavaTokenParsers
import no.antares.dbunit.DbWrapper
import no.antares.dbunit.JsonDataSet
import no.antares.dbunit.converters.CamelNameConverter
import java.io.PrintWriter

/** Grammar for parsing Dsl, examples:
 writeProps user:tommy password:secret at:jdbc:derby:derbyDB class:org.apache.derby.jdbc.EmbeddedDriver > derby.db
 the above should write a properties file for use by subsequent commands
 export props:derby.db dump flatXml {select * from users}
 refresh props:derby.db from json { "users": [ { "name": "Tommy" } ] } 
*/
class DbDslParser extends JavaTokenParsers {
  def parse( cmd: String, data: Option[String] ): Unit = {
  		// Console.err.println("parse: " + cmd)
  		parseAll( command(data), cmd )
  }

  //------------------- Component parsers, some protected for unit tests
  private def command( data: Option[String] ): Parser[Unit]	= export(data) | refresh(data) | writeProps;

  private[dbunit] def export: Parser[Unit]  = export( None )
  private[dbunit] def export( sqlIn: Option[String] ): Parser[Unit]		= "export" ~> dbSpec ~ "dump" ~ outFormat ~ opt( sql ) ^^
    {	case db~"dump"~"json"~sql => Console.print( exportJson( db, sql.getOrElse( sqlIn.getOrElse( "" ) ) ) )
    }
  ;
  private[dbunit] def refresh: Parser[Unit]  = refresh( None )
  private[dbunit] def refresh( dataIn: Option[String] ): Parser[Unit]	= "refresh" ~> dbSpec ~ "from" ~ outFormat ~ opt( data ) ^^
    {	case db~"from"~"json"~json => refreshWithJson( db, json.getOrElse( dataIn.getOrElse( "" ) ) )
    }
  ;
  def writeProps: Parser[Unit]	= "writeProps" ~> properties ^^
    { case dbProps => Console.print( dbProps ); Console.err.println("wrote: " + dbProps.toString) }
  ;

  private[dbunit] def sql: Parser[String]	= quoted;		// FixMe: repsep
  private def data: Parser[String]	= quoted;

  private def dbSpec: Parser[DbSpec]	= propFile | properties;
  private[dbunit] def propFile: Parser[DbSpec]	= "propFile:" ~> shellString ^^
    { case file => DbSpec.fromString( fromFile( new File( file ) ) ) }
  ;
  private[dbunit] def properties: Parser[DbSpec]	=
    user ~ password ~ dbUrl ~ className ~ opt( schema ) ^^	// ~ jarFile ^^
    { case usr~pwd~url~cls~Some(schema) => DbSpec( usr, pwd, url, cls, schema )
    case usr~pwd~url~cls~None => DbSpec( usr, pwd, url, cls, usr )
    }
  ;

  private[dbunit] def user: Parser[String]			= "user:" ~> shellString;
  private def password: Parser[String]	= "password:" ~> shellString;
  private def dbUrl: Parser[String]	= "at:" ~> shellString;
  private def className: Parser[String]	= "class:" ~> shellString;
  private def schema: Parser[String]	= "schema:" ~> shellString;
  // def jarFile: Parser[String]		= "jar:" ~> shellString;

  private def outFormat: Parser[Any]	= "json" | "jsonDeep" | "xml" | "flatXml";

  private def word: Parser[String]		= """\S+""".r;
  // def sentence: Parser[String]		= ("""(\S+\s*)+""").r
  private def quoted: Parser[String]			= stringLiteral	^^ ( s => unquote( s ) )
  private def shellString: Parser[String]			= quoted | word;

  //------------------- End of parsers, utilities below

  private def unquote( quoted: String ) = 	quoted.substring( 1, quoted.length() - 1 )

  private def fromFile(f: File ): String = Source.fromFile( f ).mkString;
	private def toFile( f: File, s: String ): Unit = printToFile( f )( p => { p.print( s ) } )
	private def printToFile(f: File)(op: PrintWriter => Unit) {
	  val p = new java.io.PrintWriter(f)
	  try { op(p) } finally { p.close() }
	}

	private def exportJson( spec: DbSpec, sql: String ): String = {
    val db  = new DbWrapper( spec );
    db.extractFlatJson( "data", sql ).toString( 2 )
  }
  private def refreshWithJson( spec: DbSpec, json: String ): Unit = {
    // Console.err.println( "\n\nrefreshWithJson:" +json )
    val db  = new DbWrapper( spec );
    val jsonSet	= new JsonDataSet( json, new CamelNameConverter() );
    db.refreshWithFlatJSON( jsonSet )
  }

}
