/*******************************************************************************
 * Copyright 2013 Tommy Skodje (Antares Gruppen AS - http://www.antares.no)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package no.antares.clu.dbunit

/**
@author tommy skodje
 */
object Main extends Main( new DbDslParser() ) with App {
  
  val lines	= 
	  if ( Console.in.ready() )
	    Some( Iterator.continually( Console.readLine ).takeWhile( _ != null ).addString( new StringBuilder() ).toString )
	  else
	    None
    perform( args, lines )
  Console.err.println("DONE")
}

class Main(
    val parser: DbDslParser	// may be specified for unit testing
) {
  def perform( commandLine: Array[String] ): Unit = perform( commandLine, None );
  def perform( commandLine: Array[String], data: Option[String] ): Unit = {
    val command	= CommandLineOptions.parse2dsl( commandLine )
    // Console.err.println("READ " + command + "\ndata: " + data )
    parser.parse( command, data )
  }

}
