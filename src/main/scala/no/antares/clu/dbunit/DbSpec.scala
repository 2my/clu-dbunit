/*******************************************************************************
 * Copyright 2013 Tommy Skodje (Antares Gruppen AS - http://www.antares.no)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package no.antares.clu.dbunit

import no.antares.dbunit.DbProperties

case class DbSpec(
    val user: String, val pwd: String, val url: String, val className: String, val dbSchema: String
    ) extends DbProperties( className, url, user, pwd, "" )
{
  def this( user: String, pwd: String, url: String, className: String ) = this( user, pwd, url, className, user );

  override def toString() =
    "DbSpec\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s"
    .format(user, pwd, url, className, dbSchema )
  ;

}

object DbSpec {
  def fromString( serialized: String ): DbSpec = {
    val parts	= serialized.split( "\n\t" )
    if ( parts.length < 6 )
      new DbSpec( parts(1), parts(2), parts(3), parts(4) )
    else
      new DbSpec( parts(1), parts(2), parts(3), parts(4), parts(5) )
  }
}
