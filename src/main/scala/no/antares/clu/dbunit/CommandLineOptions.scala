/*******************************************************************************
 * Copyright 2013 Tommy Skodje (Antares Gruppen AS - http://www.antares.no)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package no.antares.clu.dbunit

/*
java -jar clu.dbunit-0.1-SNAPSHOT-jar-with-dependencies.jar -p a -f "a b c" -g 'dumme dum'
Hello World: -p, a, -f, a b c, -g, dumme dum
*/
object CommandLineOptions {

	def parse2dsl( args: Array[String] ): String = {
	  args.foldLeft( "" )( (collected, args_i) => collected + quoteIfSpaced( escape( args_i ) ) + " " ).trim()
	}

	def escape( str: String ) = str.replace( "\"", "\\\"" )
	def quoteIfSpaced( str: String ) = {
	  if ( str.trim().matches( ".*\\s.*" ) )
	    "\"" + str + "\""
    else
      str
	}

}
