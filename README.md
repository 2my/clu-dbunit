Clu-dbUnit
==========

Clu-dbUnit is a command-line utility that wraps [dbUnit][dbu]. In current version it may be used to import json to database and export json from database.  

The distribution contains a self-contained jar, and an example script that may be used to run the application against JavaDb (derby), which is included in jar.  

The utility understands a DSL with 3 main commands:  
  - writeProps user:{...} password:{...} at:{db-url} class:{driver class}  
  - refresh {db-spec} from json {json}  
  - export {db-spec} dump json {sql}  
If you stream / pipe text in to utility, the streamed text will be appended to command.  
Result of commands is streamed out on std out, messages are printed on std err.  
See examples below for parameters taken by these commands

Other databases
----------
If you want to use another database, you should add the jar file with jdbc driver, and edit the script to run:  
```
java -cp clu.dbunit-*-jar-with-dependencies.jar;other-db-driver.jar "$@"
```  


See also Main class in package no.antares.clutil.hitman

Examples:
----------
Write database connection to a file (for use by subseqent commands):  
```./dbunit.sh writeProps user:u password:p at:jdbc:derby:testDb class:org.apache.derby.jdbc.EmbeddedDriver > db.props```  
Load json data into database:  
```./dbunit.sh refresh propFile:db.props from json < "{'dataset': { 'tstStrings': [{ 'id': '33', 'value': 'another'  }] } }"```  
Export data from base to json file  
```./dbunit.sh export propFile:db.props dump json "select * from tst_strings" > xported.json```  

NOTE: semicolon and strings with spaces must be escaped:  
```./dbunit.sh writeProps user:u password: \"secret password\" at:jdbc:derby:testDb\;create=true class:org.apache.derby.jdbc.EmbeddedDriver > db.props```  



[dbu]: http://www.dbunit.org "dbUnit site"